export default {

    // methods
    methods: {
        setPageClassName(className) {
            console.log('setPageClassName');
            this.clearPageClassName();
            document.getElementById('page').classList.add(className);
        },
        clearPageClassName() {
            console.log('clearPageClassName');
            document.getElementById('page').className = 'page';
        },
    },
};
