import Vue from 'vue';
import Vuex from 'vuex';

import state from './state';
import getters from './getters';
import mutations from './mutations';
import actions from './actions';

import registration from './modules/registration/index';
import profile from './modules/profile/index';
import search from './modules/search/index';

Vue.use(Vuex);

export default new Vuex.Store({
    strict: process.env.NODE_ENV === 'development',

    state,
    getters,
    mutations,
    actions,
    modules: {
        registration,
        profile,
        search,
    },
});
