// mutations
export default {
    setProfileData(state, profileData) {
        state.profileData = profileData;
    },
    updateSelectedPhoto(state, { selectedPhoto }) {
        state.profileData.mainPhoto = selectedPhoto.id;
    },
}; // mutations
