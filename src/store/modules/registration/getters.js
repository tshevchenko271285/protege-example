// getters
export default {
    StateEyes: (state) => state.eyes,
    StateHairs: (state) => state.hairs,
    StateSexes: (state) => state.sexes,
    StateCommunicationTypes: (state) => state.communicationTypes,
    StateSmokings: (state) => state.smokings,
    StateCountries: (state) => state.countries,
    }; // getters
